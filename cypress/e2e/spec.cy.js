/// <reference types="Cypress" />

const shoppingWebURL = 'https://rahulshettyacademy.com/seleniumPractise/#/'

describe('template spec', () => {
  it('passes', () => {
    cy.visit('https://example.cypress.io')
  })
})

describe('Shopping web', () => {
  it('can visit dummy website', () => {
    cy.visit(shoppingWebURL)
  })

  it('can do search', () => {
    cy.visit(shoppingWebURL)

    const keyword = 'tomato'

    cy.get('.search-keyword').type(keyword)
    cy.get('.products').find('.product').should('not.be.empty')

    // TODO: one of the result contain keyword
  })

  it('can add to cart', async () => {
    cy.visit(shoppingWebURL)

    let productName = ''

    cy.get('.products').find('.product').eq(0).then(($el) => {
      productName = cy.wrap($el).find('.product-name').text()

      cy.wrap($el).contains('ADD TO CART').click()
    })

    cy.get('.cart-icon').click()
    cy.get('.cart-items').find('.cart-item').should('not.be.empty')
    cy.get('.cart-items').find('.cart-item').should('have.text', productName)
  })
})