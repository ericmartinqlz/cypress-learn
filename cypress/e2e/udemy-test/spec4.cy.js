/// <reference types="Cypress" />

const shoppingWebURL = "https://rahulshettyacademy.com/AutomationPractice/";

describe("Fourth Test", () => {
  it("First Test Case", () => {
    cy.visit(shoppingWebURL);

    cy.get("#opentab").invoke("removeAttr", "target").click();
    // cy.url().should("include", "qaclickacademy");
    // cy.go("back");

    cy.origin("https://www.qaclickacademy.com", () => {
      cy.get("#navbarSupportedContent a[href*='about']").click();
      cy.get(".mt-50 h2").should("contain", "QAClick Academy");
    });
  });
});
