/// <reference types="Cypress" />

const shoppingWebURL = "https://rahulshettyacademy.com/seleniumPractise/#/";

describe("Second Test", () => {
  it("First Test Case", () => {
    cy.visit(shoppingWebURL);

    // Goal: Search product
    // Command - find and type
    cy.get(".search-keyword").type("ca");
    cy.wait(2000);

    // Goal: Add to Cart with specific data UI (product name)
    // Command - find from all element and click
    cy.get(".products").as("productLocator");
    cy.get("@productLocator")
      .find(".product")
      .each(($el, index, $list) => {
        const productName = $el.find("h4.product-name").text();
        if (productName.includes("Cashews")) {
          $el.find("button").click();
        }
      });

    // Goal: Open Cart
    // Command - find and click
    cy.get(".cart-icon > img").click();
    //cy.contains('PROCEED TO CHECKOUT').click()
    cy.visit("https://rahulshettyacademy.com/seleniumPractise/#/cart");

    // Goal: Order
    // Command - find and click
    cy.contains("Place Order").click();
  });
});
