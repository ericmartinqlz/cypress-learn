/// <reference types="Cypress" />

const shoppingWebURL = "https://rahulshettyacademy.com/seleniumPractise/#/";

describe("First Test", () => {
  it("First Test Case", () => {
    cy.visit(shoppingWebURL);

    // Goal: Search product
    // Command - find and type
    cy.get(".search-keyword").type("ca");
    cy.wait(2000);

    // Assertion - expect product to have length 4
    cy.get(".product:visible").should("have.length", 4);

    // Assertion - expect product to have length 4
    // but more specific selector + save as variable
    cy.get(".products").as("productLocator");
    cy.get("@productLocator").find(".product").should("have.length", 4);

    // Goal: Add to cart
    // Command - find and click
    cy.get(":nth-child(3) > .product-action > button").click();
    cy.get("@productLocator")
      .find(".product")
      .eq(2)
      .contains("ADD TO CART")
      .click();

    // Goal: Add to Cart with specific data UI (product name)
    // Command - find from all element and click
    cy.get("@productLocator")
      .find(".product")
      .each(($el, index, $list) => {
        const productName = $el.find("h4.product-name").text();
        if (productName.includes("Cashews")) {
          $el.find("button").click();
        }
      });

    // Assertion - expect brand to have text GREENKART
    cy.get(".brand").should("have.text", "GREENKART");

    // example only - print log cypress
    cy.get(".brand").then(function (logoElement) {
      cy.log(logoElement.text());
    });
  });
});
