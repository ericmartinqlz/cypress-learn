/// <reference types="Cypress" />
/// <reference types="cypress-iframe" />

import "cypress-iframe";

const shoppingWebURL = "https://rahulshettyacademy.com/AutomationPractice/";

describe("Third Test", () => {
  it("First Test Case", () => {
    cy.visit(shoppingWebURL);

    // Checkboxes
    cy.get("#checkBoxOption1")
      .check()
      .should("be.checked")
      .and("have.value", "option1");
    cy.get("#checkBoxOption1").uncheck().should("not.be.checked");
    cy.get('input[type="checkbox"]').check(["option2", "option3"]);

    // Static dropdown
    cy.get("select").select("option2").should("have.value", "option2");

    // Dynamic dropdown
    cy.get("#autocomplete").type("ind");
    cy.get(".ui-menu-item div").each(($e1, index, $list) => {
      if ($e1.text() === "India") {
        cy.wrap($e1).click();
      }
    });
    cy.get("#autocomplete").should("have.value", "India");

    // Visible Invisible
    cy.get("#displayed-text").should("be.visible");
    cy.get("#hide-textbox").click();
    cy.get("#displayed-text").should("not.be.visible");
    cy.get("#show-textbox").click();
    cy.get("#displayed-text").should("be.visible");

    // Radio button
    cy.get('[value="radio2"]').check().should("be.checked");

    // Table - row and col
    cy.get("tr td:nth-child(2)").each(($e1, index, $list) => {
      const text = $e1.text();
      if (text.includes("Python")) {
        cy.get("tr td:nth-child(2)")
          .eq(index)
          .next()
          .then(function (price) {
            const priceText = price.text();
            expect(priceText).to.equal("25");
          });
      }
    });

    // window:alert
    cy.get("#alertbtn").click();
    cy.get('[value="Confirm"]').click();
    cy.on("window:alert", (str) => {
      expect(str).to.equal(
        "Hello , share this practice page and share your knowledge"
      );
    });
    cy.on("window:confirm", (str) => {
      //Mocha
      expect(str).to.equal("Hello , Are you sure you want to confirm?");
    });

    // Force click invincible element
    cy.contains("Top").click({ force: true });
    cy.url().should("include", "top");

    // iFrame
    cy.frameLoaded("#courses-iframe");
    cy.iframe().find("li.dropdown").eq(1).invoke("show");
    cy.iframe().contains("Part time jobs").click({ force: true });
    cy.wait(2000);
    cy.iframe().find('select[name="select-jpb-type"]').select("Freelancing");
  });
});
